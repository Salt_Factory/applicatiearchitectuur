<%-- 
    Document   : welkom
    Created on : 14-nov-2018, 14:33:09
    Author     : student
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welkom</title>
    </head>
    <body>
        <h1>Welkom! U bent een klant!</h1>
        
        Sterker nog, uw naam is ${sessionScope.naam}!
        
        Sterkerder nog, uw passwoord is *****!
    </body>
</html>
