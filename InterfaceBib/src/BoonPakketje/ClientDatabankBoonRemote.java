/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BoonPakketje;

import javax.ejb.Remote;

/**
 *
 * @author student
 */
@Remote
public interface ClientDatabankBoonRemote {
    public void addWagen(int wnr, String wnaam, int prijs);
    
    public void addWagen(int lnr, String lnaam);
}
