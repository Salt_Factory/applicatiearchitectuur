/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BoonPakketje;

import javax.ejb.Remote;

/**
 *
 * @author student
 */
@Remote
public interface OnStatigeSessieBoonRemote {
    public void OnStatigeSessieBoon();
    
    public float berekenHuurPrijs(int aantalDagen, float korting);

    
    
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
