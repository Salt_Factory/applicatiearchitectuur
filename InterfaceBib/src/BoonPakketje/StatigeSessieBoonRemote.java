/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BoonPakketje;

import javax.ejb.Remote;

/**
 *
 * @author student
 */
@Remote
public interface StatigeSessieBoonRemote {
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    public void voegReservatieToe(float prijs);
    
    
    public void reset();
    
 //GETTERS EN SETTERS
    
    public void setTotalePrijs(float prijs);
    
    public void setAantalReservaties(int aantal);
    
    public float getTotalePrijs();
    
    public float getAantalReservaties();
}
