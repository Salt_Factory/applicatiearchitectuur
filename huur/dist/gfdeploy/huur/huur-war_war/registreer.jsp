<%-- 
    Document   : registreer
    Created on : 3-okt-2018, 14:50:03
    Author     : Simon Vandevelde

    Een pagina om zich te kunnen registreren!
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registratiepagina</title>
    </head>
    <body>
        <%@include file="hoofding.jspf"%>
        <h1>Hier kan u registreren!</h1>
        <form action="<c:url value='URLVanMijnServlet'/>" method="Post">
            Naam: 
            <input type="text" name="naam" value="Uw naam"><br>
            Adres:
            <input type="text" name="adres" value="Adres"><br>
            Postcode + Gemeente:
            <input type="text" name="postcode" value="Postcode">
            <input type="text" name="gemeente" value="Gemeente"><br>
            
            <input type="hidden" name="klantnummer" value="69">
            <input type="hidden" name="verstopt" value="volgend">
            <input type="Submit" value="ok">
            
        </form> 
    <jsp:include page="footer.jsp"/>
    </body>
</html>
