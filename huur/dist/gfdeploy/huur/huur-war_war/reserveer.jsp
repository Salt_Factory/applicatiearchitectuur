<%-- 
    Document   : reserveer
    Created on : 26-sep-2018, 15:07:02
    Author     : Simon Vandevelde

    Pagina die een tabel van alle beschikbare wagens weergeeft
    met de mogelijkheid deze te reserveren
--%>

<%@page import="BoonPakketje.Wagens"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%-- ArrayList nodig om het model te kunnen interpreteren --%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Reserveer!</title>
    </head>
    <body>
        <%@include file="hoofding.jspf"%>
        <h1>Welkom! Uw klantnummer is 
            <% 
                HttpSession sessie = request.getSession(true);
                
                out.print(sessie.getAttribute("klantnummer")); 
            %>
        </h1>
        <h1> Maak uw reservatie: </h1>
        <%-- EncodeURL voor wanneer cookies uitstaan --%>
        <form action="<c:url value='URLVanMijnServlet'/>" method="Post">
            <table style="width:100%">
                <tr>
                    <th>Pick-up Locatie</th>
                    <th>Type</th>
                    <th>Drop-Off Locatie</th>
                    <th>Pickup Date</th>
                    <th>Aantal Dagen</th>

                </tr>
                    <tr> 
                    <!HIER KOMEN DE BESCHIKBARE WAGENS!>

                        <td>
                           
                            <select>
                                <c:forEach var="locatie" items="${locaties}" >
                                    <option value="${locatie.getLnaam()}"> ${locatie.getLnaam()} </option> 

                                </c:forEach>
                            </select>
                        <td>
                        <td>
                            
                            <select>
                                <c:forEach var="wagen" items="${wagens}" >
                                    <option value="${wagen.getWnaam()}"> ${wagen.getWnaam()} </option> 

                                </c:forEach>
                            </select>
                            
                        </td>
                        <td>
                     
                            <select>
                                <c:forEach var="locatie" items="${locaties}" >
                                    <option value="${locatie.getLnaam()}"> ${locatie.getLnaam()} </option> 
                                </c:forEach>
                            </select>
                        </td>
                        <td>
                            <%-- simpel input veld --%>
                            <input name="pickup" >
                        </td> 
                        <td>
                            <%-- simpel input veld --%>
                            <input name="aantalDagen" >
                        </td> 
                    </tr>
                    Kortingscode?
                    <input name="kortingscode" >
                    
            </table> 
            <input type="hidden" name="verstopt" value="overzicht"> <%-- overzicht tonen --%>
            <input type="Submit" value="Reserveer!">
        </form>
        <jsp:include page="footer.jsp"/>
    </body>
</html>
