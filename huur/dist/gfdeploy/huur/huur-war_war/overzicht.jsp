<%-- 
    Document   : overzicht
    Created on : 17-okt-2018, 15:42:04
    Author     : student
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Overzicht</title>
    </head>
    <body>
        <h1>Bedankt voor uw aankoop!</h1>
        De kostprijs is: ${sessionScope.Prijs} <br>
        De kostprijs in totaal is: ${sessionScope.Totprijs}
        
        <form action="<c:url value='URLVanMijnServlet'/>" method="Post">
            <input type="hidden" name="verstopt" value="volgend">
            <input type="Submit" value="Nog een reservatie?">
            
        </form> 
    </body>
</html>
