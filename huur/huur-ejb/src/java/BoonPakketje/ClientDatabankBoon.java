/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BoonPakketje;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author student
 */
@Stateless
public class ClientDatabankBoon implements ClientDatabankBoonRemote {
    @PersistenceContext private EntityManager em;
    
    public void addWagen(int wnr, String wnaam, int prijs)
    {
     
      Wagens wagen = new Wagens();
      BigDecimal BDwnr = new BigDecimal(wnr);
      BigInteger BDprijs = BigInteger.valueOf(prijs);
      wagen.setWnr(BDwnr);
      wagen.setWnaam(wnaam);
      wagen.setPrijs(BDprijs);
     
      em.persist(wagen);
     
    }
    
    public void addWagen(int lnr, String lnaam)
    {
      Locaties locatie = new Locaties();
      BigDecimal BDlnr = new BigDecimal(lnr);
      locatie.setLnr(BDlnr);
      locatie.setLnaam(lnaam);
      
      em.persist(locatie);
    }

}
