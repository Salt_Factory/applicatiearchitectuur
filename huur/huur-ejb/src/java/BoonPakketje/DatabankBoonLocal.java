/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BoonPakketje;

import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author student
 */
@Local
public interface DatabankBoonLocal {
        
    public List getWagens();
    
    public List getLocaties();
    
    public int addKlant(String postcode, String knaam, String adres, String gemeente);
            
    public void addReservatie(int mr, Klanten knr, int dagen, int wnr, Locaties lnrVan, Locaties lnrNaar, String datumvan);
        
    public List getOverzicht(int klantnr); 
    
    public void verwijderRes(int resnr);
    
    public boolean zoekKlant(String klantnr);

}
