/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BoonPakketje;

import javax.ejb.Stateful;

/**
 *
 * @author student
 */
@Stateful
public class StatigeSessieBoon implements StatigeSessieBoonRemote {
    private static int aantalReservaties = 0;
    private static float totalePrijs = 0;
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    public void voegReservatieToe(float prijs)
    {
        aantalReservaties += 1;
        totalePrijs += prijs;
    }
    
    
    public void reset(){
        setAantalReservaties(0);
        setTotalePrijs(0);
    }
    
 //GETTERS EN SETTERS
    
    public void setTotalePrijs(float prijs){
        totalePrijs = prijs;
    }
    
    public void setAantalReservaties(int aantal){
        aantalReservaties = aantal;
    }
    
    public float getTotalePrijs(){
        return totalePrijs;
    }
    
    public float getAantalReservaties(){
        return aantalReservaties;
    }
}
    