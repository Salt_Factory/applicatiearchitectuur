/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BoonPakketje;

import javax.ejb.Stateless;


/**
 *
 * @author student
 */
@Stateless
public class OnStatigeSessieBoon implements OnStatigeSessieBoonRemote {
    
    public void OnStatigeSessieBoon(){
        
    }
    
    public float berekenHuurPrijs(int aantalDagen, float korting)
    {
        float huurprijs;
        huurprijs = (float) ((aantalDagen * 15 * (1-korting)) * 1.21 );
       
        return huurprijs;
    }
    
    
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
