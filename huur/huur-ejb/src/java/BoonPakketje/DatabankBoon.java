/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BoonPakketje;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Simon Vandevelde
 */
@Stateless
public class DatabankBoon implements DatabankBoonLocal {
    @PersistenceContext private EntityManager em;
    
    public List getWagens(){
        return em.createNamedQuery("Wagens.findAll").getResultList();
        
        
    }
    
    public List getLocaties(){
        return em.createNamedQuery("Locaties.findAll").getResultList();
        
    }
    
    public int addKlant(String postcode, String knaam, String adres, String gemeente){
        BigDecimal lklantnummer = (BigDecimal)em.createNamedQuery("Klanten.laatsteKnr").getSingleResult();
        int nklantnummer = lklantnummer.intValue() + 1;
        System.out.println("Nieuw klantnummer berekend:" + nklantnummer);
        Klanten klant = new Klanten();
        BigDecimal BDklantnummer = new BigDecimal(nklantnummer);
        BigInteger BDpostcode = new BigInteger(postcode);
        
        klant.setKnr(BDklantnummer);
        klant.setPostcode(BDpostcode);
        klant.setKnaam(knaam);
        klant.setAdres(adres);
        klant.setGemeente(gemeente);

        em.persist(klant);
        return nklantnummer;
    }
    
    public boolean zoekKlant(String klantnr)
    {
        BigDecimal knr = new BigDecimal(klantnr);
        try
        {
            Object klantentries = em.createNamedQuery("Klanten.findByKnr").setParameter("knr",knr).getSingleResult();
            return false;
        }
        catch (Exception e)
                {
                    return true;
                }
    }
    
    public void addReservatie(int mr, Klanten knr, int dagen, int wnr, Locaties lnrVan, Locaties lnrNaar, String datumvan)
    {
        Reservaties reservatie = new Reservaties();
        BigDecimal BDmr = new BigDecimal(mr);
        BigInteger BDdagen = BigInteger.valueOf(dagen);
        
        String[] parts = datumvan.split("-");
        Date BDdatumvan = new Date(Integer.parseInt(parts[0]),Integer.parseInt(parts[1]),Integer.parseInt(parts[2]));
        Date BDdatumtot = BDdatumvan;
        
        
        reservatie.setMr(BDmr);
        reservatie.setDagen(BDdagen);
        reservatie.setDatumvan(BDdatumvan);
        reservatie.setDatumres(BDdatumtot);
        reservatie.setLnrvan(lnrVan);
        reservatie.setLnrnaar(lnrNaar);
        
        em.persist(reservatie); //save het objectje
        
        
        
    }
    
    public List getOverzicht(int klantnr)
    {
        BigDecimal knr = new BigDecimal(klantnr);
        List<Reservaties> reservatielist = em.createNamedQuery("Reservaties.findByKnr").setParameter("knr",knr).getResultList();
        return reservatielist;
    }
         
   
    public void verwijderRes(int resnr)
    {
        List<Reservaties> reslist = em.createNamedQuery("Reservaties.findByMr").setParameter("mr",resnr).getResultList();
        
        //over iedere reservatie in de lijst itereren
        for (Reservaties reservatie : reslist) {
            em.remove(reservatie); //reservatie verwijderen
}
    }
    

            
      
};
            

