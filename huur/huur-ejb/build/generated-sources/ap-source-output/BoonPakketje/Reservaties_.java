package BoonPakketje;

import BoonPakketje.Klanten;
import BoonPakketje.Locaties;
import BoonPakketje.Wagens;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-11-07T14:57:36")
@StaticMetamodel(Reservaties.class)
public class Reservaties_ { 

    public static volatile SingularAttribute<Reservaties, Locaties> lnrnaar;
    public static volatile SingularAttribute<Reservaties, BigDecimal> mr;
    public static volatile SingularAttribute<Reservaties, Date> datumres;
    public static volatile SingularAttribute<Reservaties, Wagens> wnr;
    public static volatile SingularAttribute<Reservaties, Date> datumvan;
    public static volatile SingularAttribute<Reservaties, Locaties> lnrvan;
    public static volatile SingularAttribute<Reservaties, BigInteger> dagen;
    public static volatile SingularAttribute<Reservaties, Klanten> knr;

}