package Data;

import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Simon Vandevelde
 */
public class Lijstjes {
    private ArrayList locatielijst = new ArrayList();
    private ArrayList wagenlijst = new ArrayList();
    
    public Lijstjes(){
        locatielijst.add("Meise");
        locatielijst.add("Londerzeel");
        locatielijst.add("Duffel");
        locatielijst.add("Waarloos");
        
        wagenlijst.add("Mazda CX-5");
        wagenlijst.add("Fiat 500");
        wagenlijst.add("Nog een auto");
        wagenlijst.add("Een laatste auto");
}
    
    public ArrayList getLocatieLijst(){
        return this.locatielijst;
    }
    
    public ArrayList getWagenLijst(){
        return this.wagenlijst;
    }
}
