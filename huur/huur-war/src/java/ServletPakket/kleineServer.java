/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServletPakket;

import BoonPakketje.*;
import Data.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ejb.EJB;
import BoonPakketje.OnStatigeSessieBoonRemote;
import BoonPakketje.StatigeSessieBoonRemote;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Simon Vandevelde
 */
public class kleineServer extends HttpServlet {
    private float korting = (float)0.66;
    @EJB private OnStatigeSessieBoonRemote onstatigeBoon;
    @EJB private StatigeSessieBoonRemote statigeBoon;
    @EJB private DatabankBoonLocal databoon;
    

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet kleineServer</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet kleineServer at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String verstopt = request.getParameter("verstopt");
        
        //sessie aanmaken!
        HttpSession sessie = request.getSession(true);

        if (verstopt.equals("volgend"))
        {

            if (request.getParameter("naam") != null && request.getParameter("postcode") != null && 
                    request.getParameter("adres") != null && request.getParameter("gemeente") != null )
            {//nieuwe klant die wil registreren
                int klantnummer = this.registreer(request.getParameter("naam"),request.getParameter("adres"),request.getParameter("postcode"),request.getParameter("gemeente"));
                sessie.setAttribute("klantnummer",klantnummer);
            }
            
            if (sessie.getAttribute("klantnummer") == null) //de klant bestaat nog niet in de sessie
            {
                if (request.getParameter("klantnummer") != null) //de klant heeft een nummer opgegeven
                {
                    String klantnummer = request.getParameter("klantnummer");
                    sessie.setAttribute("klantnummer",klantnummer);
                }
                else //de klant heeft geen nummer opgegeven
                {
                    sessie.setAttribute("klannummer", 0); //dit zou eigenlijk error moeten geven!
                }
               
            }
            
            //opzoeken of klantnummer in sessie wel bestaat
            if (databoon.zoekKlant((String)sessie.getAttribute("klantnummer")))
            {
                
                RequestDispatcher view = request.getRequestDispatcher("reserveer.jsp");
                view.forward(request, response);
            }
            else
            {
                RequestDispatcher view = request.getRequestDispatcher("registreer.jsp");
                view.forward(request, response);
            }
            
            
            
        }
        
        else if (verstopt.equals("registreer"))
        {
            //Code die uitgevoerd wordt bij het registreren
            
            RequestDispatcher view = request.getRequestDispatcher("registreer.jsp");
            view.forward(request, response);
        }
        
        else if (verstopt.equals("index"))
        {
            RequestDispatcher view = request.getRequestDispatcher("index.jsp");
            view.forward(request, response);
        }
        
        else if (verstopt.equals("verbreek"))
        {
            sessie.invalidate();
            RequestDispatcher view = request.getRequestDispatcher("index.jsp");
            view.forward(request,response);
            statigeBoon.reset();
            
        }
        
        else if (verstopt.equals("overzicht"))
        {
            String a = request.getParameter("aantalDagen");
            System.out.println(a);
            int aantalDagen = Integer.parseInt(request.getParameter("aantalDagen"));
            
            float prijs;
            
            if ("KORTING" == getServletContext().getAttribute("Kortingscode"))
            {
                prijs = onstatigeBoon.berekenHuurPrijs(aantalDagen, korting);
            }
            else
            {
                prijs = onstatigeBoon.berekenHuurPrijs(aantalDagen,0);
            }
            sessie.setAttribute("Prijs",prijs);  //attribuut zetten
            
            statigeBoon.voegReservatieToe(prijs);
            
            float totprijs = statigeBoon.getTotalePrijs();
            sessie.setAttribute("Totprijs",totprijs);
            
            RequestDispatcher view = request.getRequestDispatcher("overzicht.jsp");
            view.forward(request,response);
            
        }
        
        
       
        //niet nodig:
        //processRequest(request, response);
    }

    //eigen initmethode
    public void init()
    {
        List locatielist = databoon.getLocaties();
        List wagenlist = databoon.getWagens();
        
        
        getServletContext().setAttribute("locaties",locatielist);
        getServletContext().setAttribute("wagens",wagenlist);
        
        System.out.println(wagenlist);
        System.out.println(locatielist);
        
        String kortingscode = getServletConfig().getInitParameter("KortingsCode");
        getServletContext().setAttribute("KortingsCode",kortingscode);
        
    }
    
    public int registreer(String naam, String adres, String postcode, String Gemeente)
    {//toevoegen van nieuwe klant
        int knr = databoon.addKlant(postcode, naam, adres, Gemeente);
        return knr;
        
    }
    
    
    
    public void volgend()
    {
        //TODO
        
    }
    
    public void reserveer()
    {
        //TODO
        
    }
    
    
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
