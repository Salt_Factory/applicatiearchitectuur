<%-- 
    Document   : footer
    Created on : 3-okt-2018, 16:04:08
    Author     : Simon Vandevelde
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Footer</title>
    </head>
    <body>
        
        <table style="width:20%">
            <c:if test="${!empty initParam.emailadres}"> 
                <c:if test="${!empty initParam.telefoonnummer}"> 
                    <tr>
                        <th>e-mail</th>
                        <th>telefoonnummer</th>
                    </tr>
                    <tr>
                        <td>${initParam.emailadres}</td>    
                        <td>${initParam.telefoonnummer}</td>
                    </tr>
                </c:if>
            </c:if>
        </table> 
        <form action="<c:url value='URLVanMijnServlet'/>" method="Post">
            <input type="hidden" name="verstopt" value="verbreek">
            <input type="Submit" value="Log uit!">
        </form>
    </body>
</html>


           
           
       