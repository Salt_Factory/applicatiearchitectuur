<%-- 
    Document   : index
    Created on : 26-sep-2018, 14:45:28
    Author     : Simon Vandevelde

    Indexpagina
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Index</title>
    </head>
    <body>
        <%@include file="hoofding.jspf"%>
        <h1>Dag (toekomstige) klant!</h1>
        <form action="<c:url value='URLVanMijnServlet'/>" method="Post">
        
            Geef hieronder uw klantnummer:<br>
            <input type="text" name="klantnummer" value=""><br>
            <input type="hidden" name="verstopt" value="volgend">
            <input type="Submit" value="Volgend">
        </form> 
        
        <h2> Moet u nog registreren? </h2>
        
        <form action="<c:url value='URLVanMijnServlet'/>" method="Post">
            <input type="Submit" value="Registreer!">
            <input type="hidden" name="verstopt" value="registreer">
        </form>
            
        <jsp:include page="footer.jsp"/>
    </body>
</html>
