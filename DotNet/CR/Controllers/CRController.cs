﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CR.Controllers
{
    public class CRController : Controller
    {
		Klanten klanten = Klanten.instance;
		Reservaties reservaties = Reservaties.instance;
        public ActionResult Index()
        {
			
            return View ();
        }

		public ActionResult Reserveer(string klantnummer)
		{
			Session["klantnummer"] = klantnummer;
			Console.WriteLine("klantnummer: " + klantnummer);

			Data data = new Data();
			Session["data"] = data;

			//initialiseren op nul / op de prijs die al in de db zit
			Session["totaleprijs"] = 0;
			if (klanten.check(int.Parse(klantnummer)))
			{
				return View();
			}
			else
			{
				return View("Registreer");
			}
		}

		public ActionResult Verbreek()
		{
			return View("Index");
		}

		public ActionResult Registreer()
		{
			Data data = new Data();
			Session["data"] = data;
			return View();
		}

		public ActionResult VerwerkRegistreer(string naam, string adres, string postcode, string gemeente)
		{
			klanten.add(naam, adres, postcode, gemeente);
			return View("Reserveer");
		}

		public ActionResult Overzicht(string locatie1, string locatie2, string voertuig, string kortingscode, string pickupdate, string aantalDagen)
		{
			Session["van"] = locatie1;
			Session["naar"] = locatie2;
			Session["wagen"] = voertuig;

			//convert pickupdate naar een datelementje

			//convert aantaldagen naar int
			Session["aantdagen"] = int.Parse(aantalDagen);


			if (kortingscode == "KEIGOEDKOOP")
			{
				Session["korting"] = true;
			}

			Console.WriteLine(voertuig + " van " + locatie1 + " naar " + locatie2 + " van " + pickupdate + " voor " + aantalDagen + " dagen.");

			int prijs = 400;//gewoon zomaar iets
			Session["prijs"] = prijs;
			int totaleprijs = (int)Session["totaleprijs"];
			Session["totaleprijs"] = totaleprijs + prijs;
			Console.WriteLine("prijs: " + prijs + "; totaleprijs: " + totaleprijs + ";");

			//nu een reservatie toevoegen
			DateTime datum = DateTime.Now;
			DateTime einddatum = datum.AddDays(double.Parse(aantalDagen));

			Data data = new Data();
			decimal wagennr = data.getWagenID(voertuig);
			decimal lnrvan = data.getLocatieID(locatie1);
			decimal lnrnaar = data.getLocatieID(locatie2);

			string klantnummer = (string)Session["klantnummer"];
			reservaties.add(decimal.Parse(klantnummer), decimal.Parse(aantalDagen), wagennr, lnrvan, lnrnaar, datum, einddatum);

			return View();
		}
        
    }
}