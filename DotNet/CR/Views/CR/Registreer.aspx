﻿<%@ Page Language="C#" MasterPageFile="~/Views/CR/meesterpagina.master" Inherits="System.Web.Mvc.ViewPage" %>
<asp:Content ID="headContent" ContentPlaceHolderID="head" runat="server">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Registratiepagina</title>
</asp:Content>
<asp:Content ID="MainContentContent" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Hier kan u registreren!</h1>
    <form action="VerwerkRegistreer" method="Post">
        Naam: 
        <input type="text" name="naam" value="Uw naam"><br>
        Adres:
        <input type="text" name="adres" value="Adres"><br>
        Postcode + Gemeente:
        <input type="text" name="postcode" value="Postcode">
        <input type="text" name="gemeente" value="Gemeente"><br>
        <input type="Submit" value="ok">
        
   	</form> 
</asp:Content>
