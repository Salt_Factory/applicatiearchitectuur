﻿<%@ Page Language="C#" MasterPageFile="~/Views/CR/meesterpagina.master" Inherits="System.Web.Mvc.ViewPage" %>
<asp:Content ID="headContent" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="MainContentContent" ContentPlaceHolderID="MainContent" runat="server">

<h1>Bedankt voor uw aankoop!</h1>
De kostprijs is: <% Response.Write(Session["prijs"]); %> <br>
De kostprijs in totaal is: <% Response.Write(Session["totaleprijs"]); %>

<form action="Reserveer" method="Post">
    <input type="hidden" name="verstopt" value="volgend">
    <input type="Submit" value="Nog een reservatie?">
    
</form> 
	
</asp:Content>
