﻿using System;
using System.Data;
using Oracle.ManagedDataAccess.Client;
namespace CR
{
	public class Reservaties
	{
		private static Reservaties _instance;
		private DataTable dt;
		private OracleDataAdapter da;
		private DbConnect db = DbConnect.instance;

		//constructor
		public Reservaties()
		{

			da = new OracleDataAdapter("select * from Reservaties", db.connect());
			dt = new DataTable("Reservaties");
			da.Fill(dt);
			OracleCommandBuilder bob = new OracleCommandBuilder(da);

			Console.WriteLine("DataTable gevuld\n");
		}

		//getter voor de singleton instance
		public static Reservaties instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = new Reservaties();
				}
				return _instance;
			}
		}

		//select een klant
		public DataRow[] select(string expressie)
		{
			return dt.Select(expressie);
		}

		//voeg een klant toe
		public void add(decimal knr, decimal dagen, decimal wnr, decimal lnrvan, decimal lnrnaar, DateTime datum, DateTime einddatum)
		{
			DataRow[] r = dt.Select("knr = max(knr)");
			decimal rnr;
			if (r.Length == 0)
			{
				rnr = 0;
			}
			else 
			{
				rnr = (decimal)r[0][0] + 1;
			}

			Console.WriteLine("Klantnummer " + knr + " met rnr" + rnr + " van  " + lnrvan + " naar " + lnrnaar + " tussen " + datum + " en " + einddatum + " wagenrn " + wnr);
			dt.Rows.Add(rnr, knr, dagen, wnr, lnrvan, lnrnaar, datum, einddatum);
			da.Update(dt);
		}

	}
}

