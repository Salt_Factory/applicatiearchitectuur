﻿using System;
using System.Data;
using Oracle.ManagedDataAccess.Client;
namespace CR
{
	public class Klanten
	{
		private static Klanten _instance;
		private DataTable dt;
		private OracleDataAdapter da;
		private DbConnect db = DbConnect.instance;

		//constructor
		public Klanten()
		{
			
			da = new OracleDataAdapter("select * from klanten", db.connect());
			dt = new DataTable("Klanten");
			da.Fill(dt);
			OracleCommandBuilder bob = new OracleCommandBuilder(da);

			Console.WriteLine("DataTable gevuld\n");
		}

		//getter voor de singleton instance
		public static Klanten instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = new Klanten();
				}
				return _instance;
			}
		}

		//select een klant
		public DataRow[] select(string expressie)
		{
			return dt.Select(expressie);
		}

		//voeg een klant toe
		public void add(string naam, string adres, string postcode, string gemeente)
		{
			DataRow[] r = dt.Select("knr = max(knr)");
			decimal knr = (decimal)r[0][0] + 1;
			Console.WriteLine("Klantnummer " + knr + " met naam " + naam + " op adres  " + adres + " postcode " + postcode + " gemeente " + gemeente);
			dt.Rows.Add(knr, decimal.Parse(postcode), naam, adres, gemeente);
			da.Update(dt);
		}

		//check if a user exists
		public bool check(int knr)
		{
			DataRow[] r = dt.Select("knr = " + knr);
			return r.Length != 0;
		}

	}
}

