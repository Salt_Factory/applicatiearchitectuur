﻿using System;
using System.Collections;
using System.Collections.Generic;
namespace CR
{
	public class Data
	{
		private ArrayList _wagenlijst = new ArrayList();
		private ArrayList _locatielijst = new ArrayList();

		public ArrayList wagenlijst
		{
			get
			{
				return _wagenlijst;
			}
			set
			{
				_wagenlijst = value;
			}
		}

		public ArrayList locatielijst
		{
			get
			{
				return _locatielijst;
			}
			set
			{
				_locatielijst = value;
			}
		}

		public Data()
		{
			wagenlijst.Add(new Wagen(0, "Fiat", 150));
			wagenlijst.Add(new Wagen(1, "Luchtballon", 15000));
			wagenlijst.Add(new Wagen(2, "Zeppelin", 150000));

			locatielijst.Add(new Locatie(0, "Meise"));
			locatielijst.Add(new Locatie(1, "Duffel"));
			locatielijst.Add(new Locatie(2, "Brussel"));
		}

		public decimal getWagenID(string wagennaam)
		{
			for (int i = 0; i < wagenlijst.Count; i++)
			{
				if (((Wagen)wagenlijst[i]).naam == wagennaam)
				{
					return i;
				}
			}
			return 0;
		}

		public decimal getLocatieID(string locatienaam)
		{
			for (int i = 0; i < wagenlijst.Count; i++)
			{
				if (((Locatie)locatielijst[i]).naam == locatienaam)
				{
					return i;
				}
			}
			return 0;
		}
	}
}

