﻿using System;
namespace CR
{
	public class Locatie
	{
		private string _naam;
		private int _nr;
		public Locatie()
		{
			_nr = 0;
			_naam = "Standaard locatie";

		}

		public Locatie(int nr, string naam)
		{
			this._nr = nr;
			this._naam = naam;
		}

		public int nr
		{
			get
			{
				return _nr;
			}
			set
			{
				_nr = value;
			}
		}

		public string naam
		{
			get
			{
				return _naam;
			}
			set
			{
				_naam = value;
			}

		}
	}
}

