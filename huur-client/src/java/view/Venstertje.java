/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;
import huurclient.*;

import javax.swing.*;
import java.awt.*;

/**
 *
 * @author student
 */
public class Venstertje extends JFrame {
    private Container pane;
    private JButton knopje;
    private JLabel labeltje;
    private JTextField textveldje;
    private Main m;
    
    public Venstertje(Main main){
        this.setM(main);
        
        pane = this.getContentPane();
        
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        knopje = new JButton("Verdergaan!");
        this.initKnopje();
        labeltje = new JLabel("Berekende prijs:");
        textveldje = new JTextField("Hoeveel dagen?");
        
        
        pane.setLayout(new BorderLayout());
        pane.add(knopje,BorderLayout.EAST);
        pane.add(labeltje,BorderLayout.NORTH);
        pane.add(textveldje,BorderLayout.CENTER);
        pane.setVisible(true);
        this.setVisible(true);
        this.pack();
    }

    private void initKnopje(){
        this.knopje.addActionListener(this.m);
    }
    
    public int getAantalDagen(){
        return Integer.parseInt(this.textveldje.getText());
    }
    
    public void updateTextveldje(String value){
        this.labeltje.setText(value);
    }
    
    public Container getPane() {
        return pane;
    }

    public void setPane(Container pane) {
        this.pane = pane;
    }

    public JButton getKnopje() {
        return knopje;
    }

    public void setKnopje(JButton knopje) {
        this.knopje = knopje;
    }

    public JLabel getLabeltje() {
        return labeltje;
    }

    public void setLabeltje(JLabel labeltje) {
        this.labeltje = labeltje;
    }

    public JTextField getTextveldje() {
        return textveldje;
    }

    public void setTextveldje(JTextField textveldje) {
        this.textveldje = textveldje;
    }
    

    public void setM(Main m){
        this.m = m;
    }
}
