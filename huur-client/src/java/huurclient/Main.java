/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package huurclient;
import BoonPakketje.OnStatigeSessieBoonRemote;
import BoonPakketje.StatigeSessieBoonRemote;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.ejb.EJB;
import view.*;
/**
 *
 * @author Simon Vandevelde
 */
public class Main implements ActionListener{

    Venstertje venster = new Venstertje(this);
    @EJB private static OnStatigeSessieBoonRemote onstatigeBoon;
    @EJB private static StatigeSessieBoonRemote statigeBoon;
    
    public static void main(String[] args) {
        Main m = new Main();
        statigeBoon.reset();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == venster.getKnopje())
        {
            float prijs = onstatigeBoon.berekenHuurPrijs(this.venster.getAantalDagen(), 0);
            statigeBoon.voegReservatieToe(prijs);
            float totprijs = statigeBoon.getTotalePrijs();
            this.venster.updateTextveldje("Prijs: " + prijs + " Totale prijs: "+totprijs);
            
        }
    }
    
}
